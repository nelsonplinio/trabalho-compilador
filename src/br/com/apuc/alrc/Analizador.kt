package br.com.apuc.alrc

class Analizador {

    companion object {
        private const val ACC = "acc"
        @JvmStatic
        val ACOES = "acoes"
        @JvmStatic
        val DESVIOS = "desvios"

        const val C_DOLAR = '$'
    }

    private val shiftRegex = "s(.+)".toRegex()
    private val reduceRegex = "r(.+)".toRegex()
    private val whiteSpaceRegex = "\\s+".toRegex()

    val producoes = listOf(
            /* 0.*/Producao("S'", "S"),
            /* 1.*/Producao("S", "E ; A"),
            /* 2.*/Producao("S", "A"),
            /* 3.*/Producao("A", "I"),
            /* 4.*/Producao("A", "E"),
            /* 5.*/Producao("I", "if E then S else S"),
            /* 6.*/Producao("E", "E + T"),
            /* 7.*/Producao("E", "F"),
            /* 8.*/Producao("E", "( E )"),
            /* 9.*/Producao("T", "T * F"),
            /*10.*/Producao("T", "F"),
            /*11.*/Producao("F", "0"),
            /*12.*/Producao("F", "1")
    )

    fun getProducoesStr() = producoes.mapIndexed {i, p ->
                val index = if (i < 10) "  $i" else "$i"

        "$index. $p"
    }.joinToString(separator = "\n")

    private val desviosMap =
            mapOf(
                //               0     1     2     3     4     5     6     7     8     9    10    11    12    13    14    15    16    17    18    19    20    21    22    23    24
                "S'" to listOf(null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null),
                "S" to listOf(    1, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null, null,   21, null, null, null,   24, null),
                "A" to listOf(    3, null, null, null, null, null, null, null, null, null,   14, null, null, null, null, null, null, null, null,    3, null, null, null,    3, null),
                "I" to listOf(    6, null, null, null, null, null, null, null, null, null,    6, null, null, null, null, null, null, null, null,    6, null, null, null,    6, null),
                "E" to listOf(    2, null, null, null, null,   12, null, null, null,   13,   15, null, null, null, null, null, null, null, null,    2, null, null, null,    2, null),
                "T" to listOf( null, null, null, null, null, null, null, null, null, null, null,   16, null, null, null, null, null, null, null, null, null, null, null, null, null),
                "F" to listOf(    4, null, null, null, null,    4, null, null, null,    4,    4,   17, null, null, null, null, null, null, null,    4,   22, null, null,    4, null)
            )

    val acoesMap = mapOf(
            //                  0     1      2     3     4     5     6      7      8     9    10    11    12    13    14    15    16     17    18    19    20    21    22    23    24
            ";"     to listOf(null, null, "s10", null, "r7", null, null, "r11", "r12", null, null, null, null, null, null, null, "r6", "r10", "r8", null, null, null, "r9", null, null),
            "if"    to listOf("s9", null,  null, null, null, null, null,  null,  null, null, "s9", null, null, null, null, null, null,  null, null, "s9", null, null, null, "s9", null),
            "then"  to listOf(null, null,  null, null, "r7", null, null, "r11", "r12", null, null, null, null,"s19", null, null, "r6", "r10", "r8", null, null, null, "r9", null, null),
            "else"  to listOf(null, null,  "r4", "r2", "r7", null, "r3", "r11", "r12", null, null, null, null, null, "r1", "r4", "r6", "r10", "r8", null, null,"s23", "r9", null, "r5"),
            "+"     to listOf(null, null, "s11", null, "r7", null, null, "r11", "r12", null, null, null,"s11","s11", null,"s11", "r6", "r10", "r8", null, null, null, "r9", null, null),
            "("     to listOf("s5", null,  null, null, null, "s5", null,  null,  null, "s5", "s5", null, null, null, null, null, null,  null, null, "s5", null, null, null, "s5", null),
            ")"     to listOf(null, null,  null, null, "r7", null, null, "r11", "r12", null, null, null,"s18", null, null, null, "r6", "r10", "r8", null, null, null, "r9", null, null),
            "*"     to listOf(null, null,  null, null, null, null, null, "r11", "r12", null, null, null, null, null, null, null,"s20", "r10", null, null, null, null, "r9", null, null),
            "0"     to listOf("s7", null,  null, null, null, "s7", null,  null,  null, "s7", "s7", "s7", null, null, null, null, null,  null, null, "s7", "s7", null, null, "s7", null),
            "1"     to listOf("s8", null,  null, null, null, "s8", null,  null,  null, "s8", "s8", "s8", null, null, null, null, null,  null, null, "s7", "s7", null, null, "s8", null),
            "$"     to listOf(null,  ACC,  "r4", "r2", "r7", null, "r3", "r11", "r12", null, null, null, null, null, "r1", "r4", "r6", "r10", "r8", null, null, null, "r9", null, "r5")
    )

    fun getTabelaFormatada() : ColunasLinhas {

        val colunas = arrayListOf("Estados")
        val linhas = arrayListOf<Array<Any>>()

        colunas.addAll(acoesMap.keys)
        colunas.addAll(desviosMap.keys)


        repeat(acoesMap.values.first().size) { index ->
            val array = arrayListOf(index.toString())

            acoesMap.keys.forEach { k ->
                array.add(acoesMap[k]!![index] ?: "")
            }

            desviosMap.keys.forEach { k ->
                array.add(desviosMap[k]!![index]?.toString() ?: "")
            }

            linhas.add(array.toTypedArray())
        }

        return ColunasLinhas(colunas.toTypedArray(), linhas.toTypedArray())
    }

    val TABELA_SLR = mapOf(
            ACOES to acoesMap,
            DESVIOS to desviosMap
    )

    private val pilha = arrayListOf(0)

    private val acoes = arrayListOf<String>()

    private var onPilhaListener: ((ArrayList<Int>) -> Unit)? = null

    private var onAcoesListener: ((acoes: ArrayList<String>) -> Unit)? = null

    private var onAccListener: (() -> Unit)? = null

    fun empilha(valor: Int) {
        pilha.add(0, valor)
        invokePilhaListener()
    }

    private fun desempilha(qnt: Int) = repeat(qnt) {
        if (pilha.size > 1)
            pilha.removeAt(0)
        invokePilhaListener()
    }

    private fun invokePilhaListener() {
        onPilhaListener?.invoke(pilha)
    }

    fun onPilhaListener(listener: (ArrayList<Int>) -> Unit) {
        this.onPilhaListener = listener
        invokePilhaListener()
    }

    private fun registrarNovaAcao(acao : String? = null) {

        if (acao != null)
            acoes.add(acao)

        onAcoesListener?.invoke(acoes)
    }

    fun onAcoesListener(listener: (acoes: ArrayList<String>) -> Unit) {
        this.onAcoesListener = listener
    }

    fun onAccListener(listener: () -> Unit) {
        onAccListener = listener
    }

    @Throws(SyntaxException::class, WordNotAcceptException::class)
    fun analizar(entrada: String) : String {
        validarEntrada(entrada)

        val simbolo = entrada.split(whiteSpaceRegex).firstOrNull() ?: throw SyntaxException("Não e possivel analizar está palavra : \"$entrada\" !!!!")

        val topoPilha = pilha.first()

        val acao = TABELA_SLR[ACOES]!![simbolo]?.get(topoPilha)?.toString() ?: throw WordNotAcceptException(entrada)

        val w = when {

            acao.startsWith(ACC) -> {
                accAction()
                entrada
            }

            shiftRegex.containsMatchIn(acao) ->
                shiftAction(entrada, simbolo, acao.removePrefix("s").toInt())

            reduceRegex.containsMatchIn(acao) ->
                    reduceAction(entrada, acao.removePrefix("r").toInt())

            else -> entrada
        }

        invokePilhaListener()

        return w
    }

    private fun accAction() {
        registrarNovaAcao("ACC")
        onAccListener?.invoke()
    }

    private fun shiftAction(entrada: String, simbolo: String, valorPraEmpilhar: Int) : String {

        empilha(valorPraEmpilhar)
        registrarNovaAcao("Empilha $valorPraEmpilhar")

        return entrada.split(whiteSpaceRegex).dropWhile { (it == simbolo || whiteSpaceRegex.containsMatchIn(it)) }.joinToString(" ")
    }

    private fun reduceAction(entrada: String, valorAcao: Int) : String {

        val producao = producoes[valorAcao]

        desempilha(producao.getQntADireita())

        val desvioValor = TABELA_SLR[DESVIOS]!![producao.esquerda]!![pilha.first()] as? Int ?: throw WordNotAcceptException(entrada)

        empilha(desvioValor)

        registrarNovaAcao("reduz conforme $desvioValor.$producao")

        return entrada
    }

    @Throws(SyntaxException::class, LexicoException::class)
    fun validarEntrada(entrada: String) {
        if (entrada.lastOrNull() != C_DOLAR)
            throw SyntaxException("""A entrada deve terminar com $C_DOLAR: "$entrada" """)

        if (!acoesMap.keys.containsAll(entrada.split(whiteSpaceRegex)))
            throw LexicoException("Erro lexico: Entrada não permitida \"$entrada\"")
    }

    fun clean() {
        pilha.clear()
        pilha.add(0)

        acoes.clear()
        registrarNovaAcao()

    }

    //Classes Aux
    data class Producao (val esquerda: String, val direita: String) {

        fun getQntADireita() = direita.split(" ").size

        override fun toString(): String {
            return """$esquerda -> $direita"""
        }
    }

    data class ColunasLinhas(val colunas: Array<Any>, val linhas: Array<Array<Any>>)

    class LexicoException(msg: String) : Exception(msg)

    class SyntaxException(msg: String) : Exception("Erro de sintax : $msg")

    class WordNotAcceptException(entrada: String) : Exception("Palavra \"$entrada\" não e aceita!!")
}