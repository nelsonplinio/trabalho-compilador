package br.com.apuc.alrc;

import kotlin.Unit;

import javax.swing.*;
import java.util.ArrayList;

public class MainWindow {
    private JPanel mainPanel;
    private JTable tabela;
    private JTextPane producoesPanel;
    private JTextPane listaDeAcoesPanel;
    private JTextPane entradaPanel;
    private JButton passoAPassoButton;
    private JButton sequencialButton;
    private JTextPane pilhaAtualPanel;
    private JButton limparAnalizadorButton;
    private JScrollPane listaDeAcoesScroll;
    private JScrollPane pilhaAtualScroll;

    private Analizador analizador;

    private boolean isAcc = false;
    private String palavraSendoAnalizada = null;

    public MainWindow() {

        setupViews();
        setupListeners();
    }

    private void setupListeners() {

        analizador.onAccListener(this::accListener);

        analizador.onAcoesListener(this::acoesListener);

        analizador.onPilhaListener(this::pilhaListener);
    }

    private Unit accListener() {
        isAcc = true;

        sequencialButton.setEnabled(false);
        passoAPassoButton.setEnabled(false);
        JOptionPane.showMessageDialog(mainPanel, "Palavra \"" + palavraSendoAnalizada + "\" foi aceita !!");

        return null;
    }

    private Unit acoesListener(ArrayList<String> acoes) {

        StringBuilder acoesStr = new StringBuilder();
        for (String acao: acoes) {
            acoesStr.append("\n").append(acao);
        }

        listaDeAcoesPanel.setText(acoesStr.toString());

        return null;
    }

    private Unit pilhaListener (ArrayList<Integer> pilhaList) {

        StringBuilder pilha = new StringBuilder();

        for (Integer valor : pilhaList)
            pilha.append("\n").append(valor);

        pilhaAtualPanel.setText(pilha.toString());

        pilhaAtualScroll.getVerticalScrollBar().setValue(0);

        return null;
    }

    private void setupViews() {
        producoesPanel.setText(analizador.getProducoesStr());

        passoAPassoButton.addActionListener(e -> passoAPassoClicked());

        sequencialButton.addActionListener(e -> sequencialClicked());

        limparAnalizadorButton.addActionListener(e -> limparAnalizadorClicked());
    }

    private void limparAnalizadorClicked() {
        isAcc = false;

        analizador.clean();

        entradaPanel.setText("");
        pilhaAtualPanel.setText("");
        palavraSendoAnalizada = null;
        listaDeAcoesPanel.setText("");
        entradaPanel.requestFocus();
        sequencialButton.setEnabled(true);
        passoAPassoButton.setEnabled(true);
    }

    private void sequencialClicked() {
        analizarPalavra(true);
    }

    private void passoAPassoClicked() {
        analizarPalavra(false);
    }

    private void analizarPalavra(boolean isSequencial) {

        if (palavraSendoAnalizada == null)
            palavraSendoAnalizada = entradaPanel.getText();

        try {
            String entrada = entradaPanel.getText();

            if (!entrada.contains("" + Analizador.C_DOLAR))
                entrada = entrada + " " + Analizador.C_DOLAR;

            if (isSequencial) {
                while (!isAcc) {
                    entrada = analizador.analizar(entrada);
                    entradaPanel.setText(entrada);
                }

            } else {
                entradaPanel.setText(analizador.analizar(entrada));
            }

        } catch (Exception e) {
            entradaPanel.requestFocus();
            sequencialButton.setEnabled(false);
            passoAPassoButton.setEnabled(false);
            JOptionPane.showMessageDialog(mainPanel, e.getMessage(), "Error", JOptionPane.ERROR_MESSAGE);
        }

    }

    public void show() {
        JFrame frame = new JFrame("Analizador");

        frame.setContentPane(mainPanel);
        frame.setSize(1000, 700);
        frame.setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
        frame.setExtendedState( frame.getExtendedState()|JFrame.MAXIMIZED_BOTH );
        frame.setVisible(true);
    }

    private void createUIComponents() {
        analizador = new Analizador();

        Analizador.ColunasLinhas cl = analizador.getTabelaFormatada();


        tabela = new JTable(cl.getLinhas(), cl.getColunas());
        tabela.setEnabled(false);
        tabela.setDragEnabled(false);

    }

}
